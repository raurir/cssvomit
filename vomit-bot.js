"use strict";
const _ = require("underscore");
const Promise = require("promise");
const Twitter = require("twitter");
const socialbot = require("./TwitterSocialBot/socialbot");
const config = require("./config");
const con = console;

const bot = () => {

  const options = {
    term: "csscool1",
    start: {
      client: true,
      stream: true,
    }
  }

  const botID = "791397954070257664";
  let client = null;

  let initClient = () => {
    client = client || socialbot.initClient(config);
  }

  let initStream = () => {
    initClient();
    con.log("initStream");
    if (options.term) {
      client.stream("statuses/filter", {track: options.term}, (stream) => {
        stream.on("data", handleTweet);
        stream.on("error", handleError);
      });
    } else {
      con.log("initStream called with no options.term...");
    }
  }

  let handleTweet = (tweet) => {
    if (tweet.user) {
      con.log("tweet", tweet.user.id_str);
      if (tweet.user.id_str === botID) {
        con.log("Got an echo of myself!", tweet.text);
      } else {
        if (tweet.text) {
          con.log("stream(user) - tweet", tweet.text);

          // con.log("tweet", tweet);

          composeReply(tweet).then(socialbot.postTweet).catch(handleError);

        } else {
          con.log("stream(user) - unknown tweet", tweet);
        }
      }
    }
  }

  let composeReply = (tweet) => {
    return new Promise((fulfill, reject) => {
      var status = null;
      try {
        status = {
          status: `@${ tweet.user.screen_name } Don't do it.`,
          in_reply_to_status_id: tweet.id
        }
      } catch(err) {
        con.log("composeReply reject 01", err);
        reject(err);
      }
      if (status) {
        fulfill(status);
      } else {
        con.log("composeReply reject 02");
        reject();
      }
    });
  }

  let handleError = (err) => {
    con.log("error", err);
  }

  if (options.start) {
    if (options.start.client) initClient();
    if (options.start.stream) initStream();
    if (options.start.checkFollowers) checkFollowers();
    if (options.start.getTweets) getTweets(options.start.getTweets);
  } else {
    con.log("no options.start passed in!");
  }

}

module.exports = bot;
bot();